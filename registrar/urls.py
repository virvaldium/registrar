from django.contrib import admin
from django.conf.urls import include, url

from users import urls as users_urls
from events import urls as events_urls
from registrations import urls as registrations_urls

api_endpoint_view = [
    url(r'^(?P<version>(v1))/', include(users_urls)),
    url(r'^(?P<version>(v1))/', include(events_urls)),
    url(r'^(?P<version>(v1))/', include(registrations_urls)),
    ]

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^api/', include(api_endpoint_view)),
]
