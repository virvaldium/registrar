from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated

from registrations.models import Registration
from registrations.api.serializer import RegistrationSerializer


class RegistrationViewSet(GenericViewSet, CreateModelMixin, ListModelMixin, RetrieveModelMixin):
    queryset = Registration.objects.all()
    serializer_class = RegistrationSerializer

    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = super(RegistrationViewSet, self).get_queryset()
        return queryset.filter(user_id=self.request.user.id)
