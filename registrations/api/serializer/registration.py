from rest_framework.serializers import ModelSerializer, HiddenField, CurrentUserDefault, PrimaryKeyRelatedField

from events.models import Event
from registrations.models import Registration


class RegistrationSerializer(ModelSerializer):
    user = HiddenField(default=CurrentUserDefault())
    event = PrimaryKeyRelatedField(queryset=Event.objects.all())

    class Meta:
        model = Registration
        fields = '__all__'
