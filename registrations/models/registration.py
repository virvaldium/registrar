from django.db import models
from django.contrib.auth.models import User

from events.models import Event


class Registration(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='users')
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='events')
    date_registration = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        app_label = 'registrations'
        db_table = 'registrations_registration'
        unique_together = (('user', 'event',),)
