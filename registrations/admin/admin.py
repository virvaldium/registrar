from django.contrib import admin

from registrations.models import Registration


class RegistrationAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'event', 'date_registration')
    search_fields = ['user', 'event', 'date_registration']


admin.site.register(Registration, RegistrationAdmin)
