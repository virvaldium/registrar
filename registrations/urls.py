from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from registrations.api.endpoint import RegistrationViewSet

registrations_router = DefaultRouter()
registrations_router.register(r'registrations', RegistrationViewSet)

urlpatterns = [
    url(r'^', include(registrations_router.urls)),
]
