from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.test import APITestCase

from registrations.models import Registration

REGISTRATION_URL = '/api/v1/registrations/'


class RegistrationTest(APITestCase):

    fixtures = ['fixtures/users.json', 'fixtures/tokens.json', 'fixtures/events.json', 'fixtures/registrations.json']

    def setUp(self):
        user = User.objects.get(pk=1)
        self.client.force_authenticate(user=user)

    def test_get_registrations_list(self):
        response = self.client.get(REGISTRATION_URL, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)

        self.assertTrue('items' in response.data.keys(), response.content)
        self.assertTrue('meta' in response.data.keys(), response.content)
        self.assertEqual(response.data['meta']['count'], 1, response.content)

    def test_double_registration(self):
        data = {
            "event": 1
        }
        response = self.client.post(REGISTRATION_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content)

    def test_correct_registration(self):
        data = {
            "event": 2
        }
        response = self.client.post(REGISTRATION_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        self.assertEqual(response.data['id'], 3, response.content)
        self.assertEqual(Registration.objects.count(), 3)

    def test_incorrect_registration(self):
        data = {
            "event": 100500     # incorrect event id
        }
        response = self.client.post(REGISTRATION_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content)
        self.assertTrue('event' in response.data.keys(), response.content)
        self.assertEqual(response.data['event'][0], 'Invalid pk "100500" - object does not exist.', response.content)
