from django.contrib import admin

from events.models import Event


class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'description', 'date_at')
    search_fields = ['title', 'description', 'date_at']


admin.site.register(Event, EventAdmin)
