from rest_framework.serializers import ModelSerializer, SerializerMethodField

from events.models import Event
from users.api.serializer import UserSerializer


class EventListSerializer(ModelSerializer):
    registered_numbers = SerializerMethodField()

    def get_registered_numbers(self, event):
        return event.users.count()

    class Meta:
        model = Event
        fields = ('id', 'title', 'description', 'date_at', 'registered_numbers')


class EventSerializer(ModelSerializer):

    users = UserSerializer(many=True)

    class Meta:
        model = Event
        fields = ('id', 'title', 'description', 'date_at', 'users')
