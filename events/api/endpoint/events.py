from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from events.models import Event
from events.api.serializer import EventListSerializer, EventSerializer


class EventViewSet(GenericViewSet, ListModelMixin, RetrieveModelMixin):
    queryset = Event.objects.prefetch_related('events__user').all()
    serializer_class = EventListSerializer

    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = EventSerializer(instance)
        return Response(serializer.data)
