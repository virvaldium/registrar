from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from events.api.endpoint import EventViewSet

events_router = DefaultRouter()
events_router.register(r'events', EventViewSet)

urlpatterns = [
    url(r'^', include(events_router.urls)),
]
