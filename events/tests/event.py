from rest_framework import status
from rest_framework.test import APITestCase

from django.contrib.auth.models import User

EVENTS_URL = '/api/v1/events/'


class EventTest(APITestCase):
    fixtures = ['fixtures/users.json', 'fixtures/tokens.json', 'fixtures/events.json', 'fixtures/registrations.json']

    def setUp(self):
        user = User.objects.get(username='user')
        self.client.force_authenticate(user=user)

    def test_get_events_list(self):
        response = self.client.get(EVENTS_URL, content_type="application/json;charset=UTF-8")
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        self.assertTrue('items' in response.data.keys(), response.content)
        self.assertTrue('meta' in response.data.keys(), response.content)
        self.assertEqual(response.data['meta']['count'], 2, response.content)
        self.assertEqual(response.data['items'][0]['registered_numbers'], 2, response.content)

    def test_get_event(self):
        response = self.client.get('{}1/'.format(EVENTS_URL), content_type="application/json;charset=UTF-8")
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEqual(len(response.data['users']), 2, response.content)

    def test_get_incorrect_event(self):
        response = self.client.get('{}100500/'.format(EVENTS_URL), content_type="application/json;charset=UTF-8")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, response.content)
