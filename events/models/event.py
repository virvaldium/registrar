from django.db import models
from django.contrib.auth.models import User


class Event(models.Model):
    title = models.CharField(max_length=128, db_index=True)
    description = models.CharField(max_length=512, null=True, blank=True)
    date_at = models.DateTimeField(db_index=True)
    users = models.ManyToManyField(User, through='registrations.Registration')

    class Meta:
        app_label = 'events'
        db_table = 'events_event'
