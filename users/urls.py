from django.conf.urls import include, url

from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter

from users.api.endpoint import UserViewSet

users_router = DefaultRouter()
users_router.register(r'users', UserViewSet)

urlpatterns = [
    url(r'^auth-token/', obtain_auth_token),
    url(r'^', include(users_router.urls)),
]
