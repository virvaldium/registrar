from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from django.contrib.auth.models import User

CREATE_USER_URL = '/api/v1/users/'


class UserTest(APITestCase):

    def test_create_user(self):
        """Ensure we can create a new user and a valid token is created with it.
        """
        data = {
            'username': 'foobar',
            'email': 'foobar@example.com',
            'password': 'somepassword'
        }

        response = self.client.post(CREATE_USER_URL, data, format='json')

        # Check returning code.
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Check returning fields
        self.assertEqual(User.objects.count(), 2)
        self.assertEqual(response.data['username'], data['username'], response.content)
        self.assertEqual(response.data['email'], data['email'], response.content)

        # Check authorization token
        user = User.objects.latest('id')
        token = Token.objects.get(user=user)
        self.assertEqual(response.data['token'], token.key)
