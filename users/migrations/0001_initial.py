from django.conf import settings
from django.db import migrations
from django.contrib.auth.hashers import make_password


def add_user(apps, schema_editor):
    User = apps.get_model(*settings.AUTH_USER_MODEL.split('.'))
    User.objects.create(
        email='admin@example.com',
        password=make_password('admin12345'),
        username='admin',
        is_staff=True,
        is_active=True,
        is_superuser=True
    )


def remove_user(apps, schema_editor):
    User = apps.get_model(*settings.AUTH_USER_MODEL.split('.'))
    User.objects.get(email='admin@example.com').delete()


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.RunPython(add_user, remove_user),
    ]
